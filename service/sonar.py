""" Sonar class file."""

import asyncio
import logging
import socket

import aioping

from service.settings import Settings


class Sonar:
    """ Guess host state using ICMP/ping and TCP calls on common ports."""
    settings = Settings()
    TIMEOUT = settings.timeout
    NB_PROCESSES = settings.concurrent_processes

    def __init__(self):
        logging.debug("Sonar Initialization")
        self.ports = [21, 22, 23, 25, 53, 80, 110, 111, 135, 143, 443, 445, 993, 995, 1723, 3306,
                      3389, 5900, 8080]
        self.sem = asyncio.Semaphore(self.NB_PROCESSES)

    async def check_port(self, host, port):
        """Check for open ports on host."""
        conn = asyncio.open_connection(host, port)
        writer = None
        try:
            _, writer = await asyncio.wait_for(conn, timeout=self.TIMEOUT)
            return True
        except asyncio.TimeoutError:
            return False
        except TimeoutError:
            return False
        except socket.gaierror as exception:
            logging.debug("Socket error - %s, %s", host, exception)
            return False
        except OSError as exception:
            logging.debug("OS Error %s, %s, %s, %s", type(exception), exception, host, port)
            return False
        except Exception as exception:
            logging.debug("Error closing  %s, %s, %s, %s", type(exception), exception, host, port)
            raise
        finally:
            if writer:
                logging.debug("Closing connection on %s:%s", host, port)
                writer.close()
                await writer.wait_closed()
                logging.debug("Closing connection on %s:%s", host, port)

    async def check_port_sem(self, host, port):
        """Check for open ports on host (using a semaphore to restrict the number of calls)."""
        async with self.sem:
            return await self.check_port(host, port)

    async def check_ping(self, host):
        """Check ping on host."""
        try:
            await aioping.ping(host, timeout=self.TIMEOUT)
        except asyncio.TimeoutError:
            return False
        except TimeoutError:
            return False
        except socket.gaierror as exception:
            logging.debug("Socket error - %s, %s", host, exception)
            return False
        except OSError as exception:
            logging.debug("OS Error %s, %s, %s", type(exception), exception, host)
            return False
        except Exception as exception:
            logging.debug("Error closing  %s, %s, %s", type(exception), exception, host)
            raise
        return True

    async def check_ping_sem(self, host):
        """Check ping on host (using a semaphore to restrict the number of calls)."""
        async with self.sem:
            return await self.check_ping(host)

    async def probe(self, host):
        """Check if an host is up/down using ICMP/ping and TCP port checking using common ports."""
        # Port checking is temporarily disables due to a unfixed bug.
        #tasks = [asyncio.ensure_future(self.check_port_sem(host, port)) for port in self.ports]
        tasks = []
        tasks.append(asyncio.ensure_future(self.check_ping_sem(host)))

        while True:
            done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)

            for task in done:
                result = task.result()
                if result:
                    # Cancel other tasks
                    if pending:
                        for p_task in pending:
                            p_task.cancel()
                        await asyncio.wait(pending)
                        pending = {}
                    return True

            if pending:
                tasks = pending
            else:
                return False
