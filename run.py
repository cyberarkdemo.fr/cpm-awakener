#!/usr/bin/python

import asyncio
import logging
import sys
import time

import requests

from entity.vault.user import User
from entity.vault.vault_client import VaultClient
from exceptions.pvwa_exception import PVWAAuthenticationException, PVWAAvailableException
from service.awakener import Awakener
from service.settings import Settings


async def async_run():
    awakener = Awakener(client)
    await awakener.awake_accounts()

if __name__ == "__main__":
    settings = Settings()

    # Logging configuration
    formatter = logging.Formatter('[%(asctime)s][%(name)s][%(levelname)s] -- %(message)s')

    logger = logging.getLogger()
    logger.setLevel(logging.getLevelName(settings.log_level))
    
    # File handler
    log_folder = settings.log_folder
    if log_folder:
        fh = logging.FileHandler('{}/cpm_awakener.log'.format(log_folder))
        fh.setFormatter(formatter)
        logger.addHandler(fh)
    
    # Stdout handler
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    # Start of program
    logging.info("==========================================")
    logging.info("=============  CPM Awakener  =============")
    logging.info("==========  By cyberarklab.fr  ===========")
    logging.info("==========================================")

    iteration = 0
    while True:
        iteration += 1
        logging.debug("  Iteration {}".format(iteration))
        logging.debug("----------------")

        cpm_awakener = User(settings.username, settings.password)
        client = VaultClient(cpm_awakener)

        try:
            client.available()
            client.login()

            # START OF Asynchronous code
            asyncio.run(async_run())
            # END OF Asynchronous code

        except requests.ConnectionError as e:
            logging.warning("Unable to login - Unknown connection error.")
            pass
        except PVWAAvailableException as e:
            logging.warning("PVWA isn't available. Waiting for next login attempt...")
            pass
        except PVWAAuthenticationException:
            logging.error("End of program to prevent locking of the account.")
            exit(1)

        # End of loop
        next_iteration_delay = settings.time_interval
        logging.debug("Next iteration in {0} seconds".format(next_iteration_delay))
        time.sleep(next_iteration_delay)

        # Reload env variables if updated
        Settings.reload()
exit()
