""" Settings class file"""

import os

from dotenv import find_dotenv, load_dotenv

from service.singleton import Singleton


class Settings(metaclass=Singleton):
    """Read .env and give access to environment variables useful to the project."""
    def __init__(self):
        Settings.reload()

    @staticmethod
    def reload():
        """Reload .env"""
        load_dotenv(find_dotenv(), override=True)

    @property
    def log_folder(self):
        """Returns the log folder path"""
        return os.getenv("LOG_FOLDER", "")

    @property
    def log_level(self):
        """Returns the log level"""
        return os.getenv("LOG_LEVEL", "INFO")

    @property
    def pvwa_url(self):
        """Returns the PVWA base url (eg: https://pvwa.domain.tld)."""
        return os.getenv("PVWA_URL")

    @property
    def username(self):
        """Returns the vault username (eg: cpm-awakener)."""
        return os.getenv("VAULT_USERNAME")

    @property
    def password(self):
        """Returns the vault password."""
        return os.getenv("VAULT_PASSWORD")

    @property
    def time_interval(self):
        """Returns the time in seconds between two scans."""
        return int(os.getenv("TIME_INTERVAL", "60"))

    @property
    def timeout(self):
        """Returns the target state verification timeout."""
        return int(os.getenv("TIMEOUT", "10"))

    @property
    def concurrent_processes(self):
        """Returns the number of concurrent processes to do target state verification."""
        return int(os.getenv("CONCURRENT_PROCESSES", "500"))

    @property
    def secret_management_reason(self):
        """Returns the justification string associated with account automatic rotation
        deactivation. """
        return os.getenv("SECRET_MGMT_REASON", "Disabled by CPM Awakener")
