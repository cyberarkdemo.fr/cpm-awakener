""" Awakener class file"""

import asyncio
import logging
from exceptions.pvwa_exception import PVWAException

from entity.vault.vault_client import VaultClient
from service.settings import Settings
from service.sonar import Sonar


class Awakener:
    """Check client's accounts address state (up/down) and choose to enable/disable automatic
    rotation in the PVWA accordingly"""

    def __init__(self, client: VaultClient):
        self.client = client
        # Initialize Sonar dynamically as it uses a Semaphore which
        # depends on the current event loop.
        self.sonar = Sonar()

    async def awake_accounts(self):
        """Update accounts automatic rotation in PVWA according to its state (up/down)"""
        tasks = []
        for account in self.client.get_accounts():
            tasks.append(asyncio.ensure_future(self.awake_account(account)))
        await asyncio.gather(*tasks)

    async def awake_account(self, account):
        """Update account automatic rotation in PVWA according to its state (up/down)"""

        # No rotation for empty address. None value resolves to 127.0.0.1
        if not account.address:
            return

        is_alive = await self.sonar.probe(account.address)  # or True
        automatic_mgmt = (account.secret_management.get('automaticManagementEnabled'))
        automatic_mgmt_reason = (account.secret_management.get('manualManagementReason'))
        allowed_to_enable = (
            automatic_mgmt_reason in (Settings().secret_management_reason, "'No Reason'"))
        try:
            if logging.getLogger().isEnabledFor(logging.DEBUG):
                target_state = 'On' if is_alive else 'Off'
                account_cpm_management = 'On' if automatic_mgmt else 'Off'

                logging.debug("Verify %s at %s. Target State: %s, CPM Automatic Rotation: "
                              "%s, Allowed to change CPM state: %s", account.username,
                              account.address, target_state, account_cpm_management,
                              allowed_to_enable)

            if is_alive and not automatic_mgmt and allowed_to_enable:
                logging.info("Enable %s at %s", account.username, account.address)
                await self.client.async_update_account_mgmt(account, 'true')
            elif not is_alive and automatic_mgmt:
                logging.info("Disable %s at %s", account.username, account.address)
                await self.client.async_update_account_mgmt(account, 'false',
                                                            Settings().secret_management_reason)
            else:
                logging.debug("Ignore %s at %s", account.username, account.address)
        except PVWAException as exception:
            logging.error('%s - An error happened with the PVWA API', exception.__class__.__name__)
