# CPM Awakener

CPM Awakener enables or disables automatic account rotation (CPM) when the associated address is not reacheable.

## How it works

CPM Awakener regularly performs the following operations:
1. **Pull** the list of accounts from the PVWA API,
2. **Verify** if the account's address answers common protocols (ping, common ports),
3. **Update** the account's automatic rotation according to the result of the previous step.

NB: The CPM Awakener **will not interfere** with accounts that have been disabled by the **CPM or manually**.

## Prerequisites

### CPM Awakener
- GNU/Linux (not tested on Windows)
- Python (>= 3.7) and required libraries (see requirements.txt)
- CyberArk account and accessible PVWA (on TCP/443)

### CyberArk account permissions
The CyberArk account requires the following permissions on monitored safes:
- List accounts
- Update account properties
- Initiate CPM account management operations

## Using Docker (recommended)
```bash
$ git clone https://gitlab.com/cyberarkdemo.fr/cpm-awakener.git
$ cd cpm-awakener

# Create the environment file and fill in the required fields.
# Be careful to NOT SET values between double quotes
$ cp .env.example .env.docker

# Run with registry image
$ docker run -d --name cpm-awakener --env-file .env.docker registry.gitlab.com/cyberarkdemo.fr/cpm-awakener
# Or build your own
$ docker build -t cpm-awakener .
$ docker run -d --name cpm-awakener --env-file .env.docker cpm-awakener
```

## Using Python (requires root)

```bash
$ git clone https://gitlab.com/cyberarkdemo.fr/cpm-awakener.git
$ cd cpm-awakener

# Install dependencies
$ sudo pip install -r requirements.txt

# Create the environment file and fill the required fields.
# Be careful to SET values between quotes
$ cp .env.example .env

$ sudo cp -rv cpm-awakener /usr/local/bin/
$ cd ..
$ rm -Rfv cpm-awakener
```

### Init service
```bash
$ cp cpm-awakener.sh /etc/init.d/cpm-awakener
$ chkconfig cpm-awakener on

$ service cpm-awakener start
```

### Systemd service
```bash
$ sudo cp cpm-awakener.service /lib/systemd/system/
$ systemctl enable cpm-awakener.service

$ systemctl start cpm-awakener
```

### Execute manually

```bash
$ cd /usr/local/bin/cpm-awakener
$ sudo python run.py
```

Logs are stored in the Log folder configured in .env  
With Init, logs are also available in /var/log/cpm-awakener.log
