""" VaultClient class file"""

import json
import logging
from exceptions.pvwa_exception import (PVWAAccountException,
                                       PVWAAccountPasswordException,
                                       PVWAAuthenticationException, PVWAAvailableException)

import aiohttp
import requests

from entity.vault.account import Account
from service.settings import Settings


class VaultClient:
    """ Representation of a Vault connection."""
    URI_CHECK = "{0}/PasswordVault/API/settings/authentication"
    URI_LOGIN = "{0}/PasswordVault/API/Auth/{1}/Logon"
    URI_ACCOUNTS = "{0}/PasswordVault/api/Accounts"
    URI_ACCOUNT = "{0}/PasswordVault/api/Accounts/{1}"
    URI_ACCOUNT_PASSWORD = "{0}/PasswordVault/api/Accounts/{1}/Password/Retrieve"

    def __init__(self, user):
        logging.debug("Vault API Client initialized")
        self.url = Settings().pvwa_url
        self.user = user
        self.token = None
        self._headers = {
            'Content-Type': 'application/json'
        }

    @property
    def headers(self) -> dict:
        """ Get required request headers."""
        headers = self._headers.copy()
        if self.token is not None:
            headers['Authorization'] = self.token.replace('\r','')
        return headers

    @headers.setter
    def headers(self, value):
        """ Set headers manually."""
        self._headers = value

    ###########################################################################
    # PVWA API CALLS
    ###########################################################################

    def available(self):
        check_url = VaultClient.URI_CHECK.format(self.url)
        response = requests.request("GET", check_url)
        if response.status_code == requests.codes.ok:  # pylint: disable=no-member
            logging.debug("%s logged in successfully", self.user.username)
            self.token = response.text.strip('"')
        else:
            raise PVWAAvailableException(
                "PVWA {0} is not available for authentication".format(self.url))

    def __pre_login(self):
        """ Called at the beginning of login/async_login"""
        login_url = VaultClient.URI_LOGIN.format(self.url, self.user.type)
        payload = {
            "username": self.user.username,
            "password": self.user.password
        }

        return login_url, payload

    async def async_login(self):
        """(Async) Authenticate to the vault and update headers accordingly."""
        login_url, payload = self.__pre_login()

        async with aiohttp.ClientSession() as session:
            async with session.post(login_url, headers=self.headers,
                                    data=json.dumps(payload)) as response:
                self.__post_login(await response.text(), response.status)
        return self.token

    def login(self) -> str:
        """Authenticate to the vault and update headers accordingly."""
        login_url, payload = self.__pre_login()

        response = requests.request("POST", login_url, headers=self.headers,
                                    data=json.dumps(payload))
        self.__post_login(response.text, response.status_code)
        return self.token

    def __post_login(self, response_text, status_code):
        """ Called at the end of login/async_login."""
        if status_code == requests.codes.ok:  # pylint: disable=no-member
            logging.debug("%s logged in successfully", self.user.username)
            self.token = response_text.strip('"')

        else:
            raise PVWAAuthenticationException(
                "Authentication failed for {0}.".format(self.user.username))

    def get_accounts(self, params=None, with_password=False) -> list:
        """ Fetch all accounts from the vault."""
        url = VaultClient.URI_ACCOUNTS.format(self.url)
        response = requests.request("GET", url, headers=self.headers, data={}, params=params)
        # Get a list of accounts. Each account is a list
        accounts_as_array = json.loads(response.text)

        # Create account object with output
        accounts = []
        for account_as_array in accounts_as_array['value']:
            # We use the name as the id. The id parameter fetched is specific to the Vault API.
            if account_as_array.get('name') is None:
                raise PVWAAccountException('Account name is missing. Can\'t instantiate.')

            # Fetch the account password.
            if with_password:
                account_as_array['password'] = self.get_account_password(account_as_array.get('id'))
            else:
                account_as_array['password'] = ''

            accounts.append(Account(**account_as_array))
        return accounts

    def get_account_password(self, account_name: str) -> str:
        """ Fetch the account's password"""
        url = VaultClient.URI_ACCOUNT_PASSWORD.format(self.url, account_name)
        response = requests.request("POST", url, headers=self.headers, data={})
        if response.status_code == requests.codes.ok:  # pylint: disable=no-member
            # Remove quotes from the received secret. This is specific to Vault responses
            return response.text.strip('"')

        raise PVWAAccountPasswordException("Account password could not be fetched.")

    def __pre_update_account_mgmt(self, account: Account, state: str, disable_message=None):
        """ Called at the beginning of update_account_mgmt/async_update_account_mgmt"""
        url = VaultClient.URI_ACCOUNT.format(self.url, account.id)
        payload = [
            {
                "op": "replace",
                "path": "/secretManagement/automaticManagementEnabled",
                "value": "{0}".format(state)
            }
        ]
        # Write a Disabled Message
        if state == "false":
            payload.append(
                {
                    "op": "replace",
                    "path": "/secretManagement/manualManagementReason",
                    "value": "{0}".format(disable_message)
                }
            )
        return url, payload

    async def async_update_account_mgmt(self, account: Account, state: str, disable_message=None):
        """(Async) Update the management state of an account."""
        url, payload = self.__pre_update_account_mgmt(account, state, disable_message)

        async with aiohttp.ClientSession() as session:
            async with session.patch(url, headers=self.headers,
                                     data=json.dumps(payload)) as response:
                self.__post_update_account_mgmt(account, response.status)

        return response

    def update_account_mgmt(self, account: Account, state: str, disable_message=None):
        """Update the management state of an account."""
        url, payload = self.__pre_update_account_mgmt(account, state, disable_message)

        response = requests.request("PATCH", url, headers=self.headers, data=json.dumps(payload))
        self.__post_update_account_mgmt(account, response.status_code)

        return response

    def __post_update_account_mgmt(self, account, status_code):
        """ Called at the end of update_account_mgmt/async_update_account_mgmt"""
        if status_code == requests.codes.ok:  # pylint: disable=no-member
            logging.debug("Successfully updated %s", account)
        else:
            raise PVWAAccountException(
                "Status code {} - Failed to update {}.".format(status_code, account))
