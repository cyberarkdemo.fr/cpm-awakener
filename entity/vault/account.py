""" Account class file"""
import logging


# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=too-few-public-methods
class Account:
    """ Representation of a Vault Account."""
    def __init__(self, id, name, safeName, platformId, password=None, userName=None, address=None,
                 secretType=None, secretManagement=None, platformAccountProperties=None,
                 createdTime=None, **kwargs):
        # Mandatory attributes
        self.id = id
        self.name = name
        self.password = password
        self.safe = safeName
        self.platform = platformId

        # Common attributes
        self.username = userName
        self.address = address
        self.secret_type = secretType
        self.secret_management = secretManagement
        self.platform_account_properties = platformAccountProperties
        self.created_time = createdTime

        # Other attributes
        self.attributes = {}
        for key, value in kwargs.items():
            logging.debug("Account parameter %s is not well known.", key)
            self.attributes[key] = value

    def __str__(self):
        return "{} - {}".format(self.username, self.address)
