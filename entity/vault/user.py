""" User class file"""

# pylint: disable=too-few-public-methods
# pylint: disable=redefined-builtin
class User:
    """ Representation of a Vault User."""
    def __init__(self, username, password, type="CyberArk"):
        self.username = username
        self.password = password
        self.type = type

    def __str__(self):
        return self.username
