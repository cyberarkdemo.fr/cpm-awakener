"""PVWA exception classes"""


class PVWAException(Exception):
    """Base exception class"""


class PVWAAvailableException(PVWAException):
    """Availability exception class"""


class PVWAAuthenticationException(PVWAException):
    """Authentication exception class"""


class PVWAAccountException(PVWAException):
    """Exception class for /accounts/ api calls"""


class PVWAAccountPasswordException(PVWAException):
    """Exception class for /accounts/{}/password/retrieve api calls"""
